# Remove running instance of the database
docker rm -f hsqldb

# Start new instance of hsqldb.
docker run --name hsqldb -d -p 9001:9001 datagrip/hsqldb:2.3.4