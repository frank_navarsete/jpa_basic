package com.glitchcube;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: glitchcube.com
 */
@Entity // https://docs.oracle.com/javaee/7/tutorial/persistence-intro001.htm#BNBQA
@Table(name = "T_ORDER") //Order is a reserved word. Hence add T_ to change the table name.
@NamedQueries({ //https://docs.oracle.com/javaee/7/tutorial/persistence-querylanguage002.htm#BNBRG
        @NamedQuery(name = "Order.findById", query = "select o from Order o where o.id=:id")
})
public class Order {

    // https://docs.oracle.com/javaee/7/tutorial/persistence-basicexamples001.htm#GIQQY
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    // https://docs.oracle.com/javaee/7/tutorial/persistence-intro001.htm#BNBQH
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }


    public void addItem(Item item) {
        if (item != null) {
            item.setOrder(this);
            items.add(item);
        }
    }

    public long getId() {
        return id;
    }
}
