package com.glitchcube;

import javax.persistence.*;

/**
 * @author: glitchcube.com
 */
@Entity
@Table
@NamedQueries({
        @NamedQuery(name = "Item.findById", query = "select i from Item i where i.id=:id")
})
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    public Item() {
    }

    public Item(String name) {
        this.name = name;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }
}
