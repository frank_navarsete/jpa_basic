package com.glitchcube;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

/**
 * @author: glitchcube.com
 */
public class PersonTest {

    private EntityManager em;
    private EntityTransaction tx;

    @Before
    public void setup() {
        this.em = Persistence.createEntityManagerFactory("integration-test").createEntityManager();
        this.tx = this.em.getTransaction();

        tx.begin();
    }

    @Test
    public void storePerson() {
        em.persist(new Person("Frank"));
        em.flush();
        List personList = em.createQuery("select p from Person p").getResultList();
        System.out.println(personList.size());

    }

    @Test
    public void storeMorePerson() {
        em.persist(new Person("Frank"));
        em.flush();
        List personList = em.createQuery("select p from Person p").getResultList();
        System.out.println(personList.size());
    }

    @After
    public void after() {
        tx.commit();

    }

}