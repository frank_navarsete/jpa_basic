package com.glitchcube;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

/**
 * @author: glitchcube.com
 */
public class OrderTest {


    private EntityManager em;
    private EntityTransaction tx;

    @Before
    public void setup() {
        this.em = Persistence.createEntityManagerFactory("integration-test").createEntityManager();
        this.tx = em.getTransaction();

        tx.begin();
    }

    @Test
    public void shouldPersistOrderAndItems() {
        Order order = new Order();
        Item item1 = new Item("Computer");
        Item item2 = new Item("iPad");
        order.addItem(item1);
        order.addItem(item2);
        em.persist(order);
        em.flush();

        List<Order> orderList = em.createQuery("select o from Order o").getResultList();
        Order orderFromDB = orderList.get(0);
        System.out.println("how many items are persisted: " + orderFromDB.getItems().size());

    }

    @Test
    public void shouldUpdateItemWhenPersistingOrderObject() {
        Order order = new Order();
        Item item = new Item("Computer");
        order.addItem(item);

        em.persist(order);
        em.flush();

        List<Order> orderList = em.createNamedQuery("Order.findById").setParameter("id", order.getId()).getResultList();

        assertThat(orderList.size(), is(1));
        Order orderFromDB = orderList.get(0);

        assertThat(orderFromDB.getItems().size(), is(1));
        Item itemFromDB = orderFromDB.getItems().get(0);

        assertThat(itemFromDB.getName(), is("Computer"));
        itemFromDB.setName("Mac");

        em.merge(orderFromDB);
        em.flush();

        Item itemUpdated = (Item) em.createNamedQuery("Item.findById").setParameter("id", itemFromDB.getId()).getResultList().get(0);

        assertThat(itemUpdated.getName(), is("Mac"));
    }

    @Test
    public void shouldDeleteItemFromOrder(){
        Order order = new Order();
        order.addItem(new Item("Computer"));

        em.persist(order);
        em.flush();

        Order orderFromDB = (Order) em.createNamedQuery("Order.findById").setParameter("id", order.getId()).getResultList().get(0);

        assertThat(orderFromDB.getItems().size(), is(1));
        orderFromDB.getItems().remove(0);

        em.merge(orderFromDB);
        em.flush();

        List itemList = em.createQuery("select i from Item i").getResultList();
        assertThat(itemList.size(), is(0));

    }

    @After
    public void after() {
        tx.commit();
    }
}